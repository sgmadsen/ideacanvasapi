module.exports.tally = function (values, scores) {
  var total = 0;
  for(var i in scores){
    total += values[scores[i]];
  }
  return total;
};
