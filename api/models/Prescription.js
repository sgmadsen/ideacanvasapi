/**
* Prescription.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    elevator: {
      model: 'ElevatorMessage',
      via: 'prescription'
    },
    solution_name: {
      type: 'string'
    },
    solution_description: {
      type: 'string'
    },
    distinction: {
      type: 'number',
      defaultsTo: 3,
      min: 1,
      max: 5
    },
    innovation: {
      type: 'number',
      defaultsTo: 3,
      min: 1,
      max: 5
    },
    innovation_name: {
      type: 'string'
    },
    differentiators: {
      type: 'string'
    },
    experience: {
      type: 'number',
      defaultsTo: 3,
      min: 1,
      max: 5
    },
    labs_platform: {
      type: 'number',
      defaultsTo: 1,
      min: 1,
      max: 5
    },
    total: {
      type: 'number'
    }
  },

  afterCreate: function (values, cb) {
    var scores = ['distinction', 'innovation', 'experience', 'labs_platform'];

    values.total = sails.services.tally(values, scores);
    cb();
  }
};

