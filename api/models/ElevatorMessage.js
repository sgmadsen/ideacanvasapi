/**
* ElevatorMessage.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    idea: {
      model: 'Idea'
    },
    pain: {
      model: 'Pain'
    },
    potential: {
      model: 'Potential'
    },
    prescription: {
      model: 'Prescription'
    },
    positioning: {
      model: 'Positioning'
    },
    exit: {
      model: "Exit"
    },
    total: {
      type: 'number'
    }
  }
};

