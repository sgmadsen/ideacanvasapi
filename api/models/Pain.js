/**
* Pain.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    elevator: {
      model: 'ElevatorMessage',
      via: 'pain'
    },
    target_customer: {
      type: 'string'
    },
    target_customer_job: {
      type: 'string'
    },
    target_customer_pain: {
      type: 'string'
    },
    pain_size: {
      type: 'number',
      defaultsTo: 3,
      min: 1,
      max: 5
    },
    pain_frequency: {
      type: 'number',
      defaultsTo: 3,
      min: 1,
      max: 5
    },
    total: {
      type: 'number'
    }
  },

  afterCreate: function (values, cb) {
    var scores = ['pain_size', 'pain_frequency'];

    values.total = sails.services.tally(values, scores);
    cb();
  }
};

