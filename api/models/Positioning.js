/**
* Positioning.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    elevator: {
      model: 'ElevatorMessage',
      via: 'positioning'
    },
    market_landscape: {
      type: 'number',
      defaultsTo: 3,
      min: 1,
      max: 5
    },
    market_gorilla: {
      type: 'number',
      defaultsTo: 1,
      min: 1,
      max: 5
    },
    gorilla: {
      type: 'string'
    },
    casualties: {
      type: 'string'
    },
    entry_strategy: {
      type: 'number',
      defaultsTo: 3,
      min: 1,
      max: 5
    },
    entry_strategy_body: {
      type: 'string'
    },
    total: {
      type: 'number'
    }
  },

  afterCreate: function (values, cb) {
    var scores = ['market_landscape', 'market_gorilla', 'entry_strategy'];

    values.total = sails.services.tally(values, scores);
    cb();
  }
};

