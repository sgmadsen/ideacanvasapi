/**
* Idea.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    owner: {
      model: 'User'
    },
    inventor: {
      model: 'Inventor'
    },
    submitted: {
      type: 'date'
    },
    labs_response: {
      type: 'string',
      defaultsTo: 'Pitch Deck'
    },
    elevator_message: {
      collection: 'ElevatorMessage'
    }
  }
};

