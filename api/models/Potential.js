/**
* Potential.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
    elevator: {
      model: 'ElevatorMessage',
      via: 'potential'
    },
    customer_count: {
      type: 'number',
      defaultsTo: 3,
      min: 1,
      max: 5
    },
    sustainability: {
      type: 'number',
      defaultsTo: 3,
      min: 1,
      max: 5
    },
    market_growth: {
      type: 'number',
      defaultsTo: 3,
      min: 1,
      max: 5
    },
    optimism: {
      type: 'number',
      defaultsTo: 3,
      min: 1,
      max: 5
    },
    market_influences: {
      type: 'string'
    },
    financial_potential: {
      type: 'number',
      min: 4,
      max: 20,
      defaultsTo: 12
    },
    total: {
      type: 'number'
    }
  },

  afterCreate: function (values, cb) {
    var scores = ['customer_count', 'sustainability', 'market_growth', 'optimism', 'financial_potential'];

    values.total = sails.services.tally(values, scores);
    cb();
  }
};

