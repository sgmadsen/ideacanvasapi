module.exports.policies = {
  '*': [
    'basicAuth',
    'passport',
    'sessionAuth',
    'ModelPolicy',
    'AuditPolicy',
    'OwnerPolicy',
    'PermissionPolicy',
    'RolePolicy',
    'CriteriaPolicy'
  ],

  IdeaController: {
    '*': []
  },
  InventorController: {
    '*': []
  },
  PainController: {
    '*': []
  },
  AuthController: {
    '*': [ 'passport' ]
  }
};
